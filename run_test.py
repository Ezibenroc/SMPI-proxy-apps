'''
Run an SMPI simulation twice, once in "online" mode to generate a trace, then in "offline" mode using this trace.
The simulated times of both simulations are compared. An error is thrown if either simulation fails or if the times do
not match.

Syntax:
python run_test.py <nb_ranks> <host_file> <platform_file> <program args>

Example:
python $WORKSPACE/run_test.py 16 $PLATFORMDIR/cluster_hostfile.txt $PLATFORMDIR/cluster_crossbar.xml ./xhpl
'''

import subprocess
import sys
import logging
import argparse
import os
import re

TRACE_FILE = '/tmp/SMPI_trace'

# Logging code, taken from https://stackoverflow.com/a/56944256/4110059
class CustomFormatter(logging.Formatter):
    grey = "\x1b[38;20m"
    yellow = "\x1b[33;20m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    green = "\x1b[32;20m"
    bold_green = "\x1b[32;1m"
    reset = "\x1b[0m"
    # format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s (%(filename)s:%(lineno)d)"
    format = "%(asctime)s - %(levelname)s - %(message)s"
    FORMATS = {
        logging.DEBUG: grey + format + reset,
        logging.INFO: green + format + reset,
        logging.WARNING: yellow + format + reset,
        logging.ERROR: red + format + reset,
        logging.CRITICAL: bold_red + format + reset
    }
    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)

class SimulationError(Exception):
    pass

logger = logging.getLogger("SMPI test")
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(CustomFormatter())
logger.addHandler(ch)

def get_simtime(output):
    regex = re.compile('Simulated time: (?P<number>-?\ *[0-9]+\.?[0-9]*(?:[Ee]\ *-?\ *[0-9]+)?)')
    for line in output.split('\n'):
        match = regex.search(line)
        if match:
            return float(match.group('number'))
    logger.critical('Could not find simulation time in output')
    logger.debug(output)
    raise SimulationError

def run_simulation(nb_hosts, hostfile, platformfile, *args):
    cmd = ['smpirun',  '--cfg=smpi/display-timing:yes', '-np',  str(nb_hosts),  '-hostfile',  hostfile,
            '-platform', platformfile, *args]
    proc = subprocess.run(cmd, capture_output=True)
    stdout = proc.stdout.decode().strip()
    stderr = proc.stderr.decode().strip()
    if proc.returncode != 0:
        logger.error(f'With command {" ".join(cmd)}')
        if stdout:
            logger.debug(stdout)
        if stderr:
            logger.debug(stderr)
        raise SimulationError
    if 'warning' in stdout.lower():
        logger.warning('')
        logger.debug(stdout)
    if 'warning' in stderr.lower():
        logger.warning('')
        logger.debug(stderr)
    t = get_simtime(stderr)
    return t

def run_online_simulation(nb_hosts, hostfile, platformfile, *args):
    args = ['-trace-ti', f'--cfg=tracing/filename:{TRACE_FILE}', *args]
    t = run_simulation(nb_hosts, hostfile, platformfile, *args)
    logger.info('Online simulation:  SUCCESS')
    return t

def remove_trace_files(main_file):
    with open(main_file) as f:
        files = [l.strip() for l in f.readlines()]
    for f in [main_file, *files]:
        os.remove(f)
    dirname = os.path.dirname(files[0])
    os.rmdir(dirname)

def run_offline_simulation(nb_hosts, hostfile, platformfile):
    args = ['-replay', TRACE_FILE]
    t = run_simulation(nb_hosts, hostfile, platformfile, *args)
    logger.info('Offline simulation: SUCCESS')
    return t

def main(nb_hosts, hostfile, platformfile, *args):
    LIMIT_RATIO = 1e-3  # let's accept a 0.1% difference, maybe this needs to be changed
    t_online = run_online_simulation(nb_hosts, hostfile, platformfile, *args)
    t_offline = run_offline_simulation(nb_hosts, hostfile, platformfile)
    ratio = t_offline/t_online
    if abs(1-ratio) > LIMIT_RATIO:
        logger.warning(f'Different times for online and offline simulation ({(1-ratio)*100:.2f}% difference)')
        logger.debug(f'Online  simulated time: {t_online} seconds')
        logger.debug(f'Offline simulated time: {t_offline} seconds')
        if abs(1-ratio) > LIMIT_RATIO*100:
            logger.error('Time difference is way too large, there must be a problem.')
            raise SimulationError


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Test runner for SMPI proxy apps')
    parser.add_argument('nb_hosts', type=int)
    parser.add_argument('hostfile', type=str)
    parser.add_argument('platformfile', type=str)
    parser.add_argument('program_args', type=str, nargs='*')
    args = parser.parse_args()
    pargs = sum([c.split() for c in args.program_args], [])
    try:
        main(args.nb_hosts, args.hostfile, args.platformfile, *pargs)
    except SimulationError:
        retcode = 1
    else:
        retcode = 0
    remove_trace_files(TRACE_FILE)
    sys.exit(retcode)
